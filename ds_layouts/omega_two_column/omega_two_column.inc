<?php
function ds_omega_two_column() {
  return array(
    'label' => t('Omega Two Column'),
    'regions' => array(
      'left' => t('Left'),
      'right' => t('Right')
    ),
    // Add this line if there is a default css file.
    'css' => TRUE,
    // Add this line if you're using DS 2.x for icon preview
    'image' => FALSE,
  );
}
?>
